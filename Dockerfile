FROM node:lts-alpine as build
COPY . /build
WORKDIR /build
RUN yarn install --forzen-lockfile && \
yarn build && \
rm -rf node_modules

FROM node:lts-alpine
COPY --from=build /build /app
WORKDIR /app
RUN yarn install --forzen-lockfile --prod
CMD yarn start:prod