## The Software House - Node.js Developer recruitment task

Please use node < 14.x.x because there is bug in ts-node-dev with ts.watch  
also it's required to install and run the project with yarn

## Good way to do...
Getting movie for recommendation should be done on database, passing every movie to algorithm is not optimal,
becouse this task explicity say its should stay as json file thats why I choose to pass every movie to algorithm.

## Scripts
yarn start - start project for development  
yarn start:watch - start project for development in watch mode  
yarn start:prod - start project for production(compiled code)  
yarn build - build project to ./dist.  
yarn test - run jest.  
yarn lint - run eslint.  
yarn format - run prettier.  

## Endpoints

GET `/` - shows 'Hi!'  
GET `/api/movies` - shows all movies from db  
POST `/api/movies` - validate and add movie to db  
GET `/api/movies/:id` - shows specific movie from db  
PATCH `/api/movies/:id` - update specific movie in db  
GET `/api/movies/recommend` - give recommended movie from db  

PATCH `/api/movies/:id` json body
```json
    {
      "title": string, // optional, max 255 chars
      "year": number, // optional, min 0
      "runtime": number, // optional, min 0
      "genres": string[] | Genres[], // optional, list of genres `/api/genres`
      "director": string, // optional, max 255 chars
      "actors": string, // optional
      "plot": string, // optional
      "posterUrl": string, // optional
    }
```


GET `/api/movies/recommend` json body
```json
{
  "runtime": number, // optional
  "genres": string[] | Genres[] // optional
}
```
POST `/api/movies` json body
```json
    {
      "title": string, // required, max 255 chars
      "year": number, // required, min 0
      "runtime": number, // required, min 0
      "genres": string[] | Genres[], // required, list of genres `/api/genres`
      "director": string, // required, max 255 chars
      "actors": string, // optional
      "plot": string, // optional
      "posterUrl": string, // optional
    }
```