import { createContainer, InjectionMode, asClass, AwilixContainer } from 'awilix';
import { InMemoryMovieRepository } from './db/InMemory/InMemoryMoviesRepository';
import { InMemoryGenresRepository } from './db/InMemory/InMemoryGenresRepository';
import { MoviesRepository } from './db/MoviesRepository';
import { GenresRepository } from './db/GenresRepository';
import { MoviesService } from './server/services/MoviesServices';
import { RecommendService } from './server/services/RecommendService';
import { TestGenresRepository } from './db/Test/TestGenresRepository';
import { TestMovieRepository } from './db/Test/TestMoviesRepository';
interface Dependencies {
  moviesRepository: MoviesRepository;
  genresRepository: GenresRepository;
  moviesService: MoviesService;
  recommendService: RecommendService;
}
export const getContainer = (): AwilixContainer<Dependencies> => {
  const container = createContainer<Dependencies>({ injectionMode: InjectionMode.CLASSIC });

  container.register({
    moviesRepository: asClass(InMemoryMovieRepository).singleton(),
    genresRepository: asClass(InMemoryGenresRepository).singleton(),
    moviesService: asClass(MoviesService).scoped(),
    recommendService: asClass(RecommendService).singleton(),
  });
  return container;
};

export const getTestContainer = (): AwilixContainer<Dependencies> => {
  const test = createContainer<Dependencies>({ injectionMode: InjectionMode.CLASSIC });
  test.register({
    moviesRepository: asClass(TestMovieRepository).singleton(),
    genresRepository: asClass(TestGenresRepository).singleton(),
    moviesService: asClass(MoviesService).scoped(),
    recommendService: asClass(RecommendService).singleton(),
  });
  return test;
};
