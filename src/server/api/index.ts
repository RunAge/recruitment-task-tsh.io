import { Router } from 'express';
import { scopePerRequest, loadControllers } from 'awilix-express';
import { getContainer } from '../../container';
import { json as jsonBodyParser } from 'body-parser';

const router = Router({ mergeParams: true });
router.use(jsonBodyParser());
router.use(scopePerRequest(getContainer()));
router.use(loadControllers('*.controller.*', { cwd: __dirname }));

export const api = router;
