import Joi from 'joi';
import { NextFunction, Request, Response } from 'express';
import { ErrorCodes } from '../../../ErrorCodes';
import { MovieDto } from '../dto/movie.dto';
import { Genres } from '../../../db/models/GenresModel';

const CreateMovieScheme = Joi.object<MovieDto>({
  title: Joi.string().required().max(255),
  year: Joi.number().min(0).strict().required(),
  runtime: Joi.number().min(0).strict().required(),
  director: Joi.string().required().max(255),
  genres: Joi.array()
    .items(Joi.string().valid(...Object.values(Genres)))
    .min(1)
    .required(),
  actors: Joi.string().optional(),
  plot: Joi.string().optional(),
  posterUrl: Joi.string().optional(),
});

export const CreateMovieMiddlewareValidator = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    await CreateMovieScheme.validateAsync(req.body);
    next(false);
  } catch (error) {
    next({ name: ErrorCodes.Validation, message: error.message, error: error });
  }
};
