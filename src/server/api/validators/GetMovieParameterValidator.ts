import Joi from 'joi';
import { NextFunction, Request, Response } from 'express';
import { ErrorCodes } from '../../../ErrorCodes';

const GetMovieParameterScheme = Joi.object<{ id: number }>({
  id: Joi.number().min(1).required(),
});

export const GetMovieParameterMiddlewareValidator = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    await GetMovieParameterScheme.validateAsync(req.params);
    next(false);
  } catch (error) {
    next({ name: ErrorCodes.Validation, message: error.message, error: error });
  }
};
