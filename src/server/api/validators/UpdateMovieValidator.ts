import Joi from 'joi';
import { NextFunction, Request, Response } from 'express';
import { ErrorCodes } from '../../../ErrorCodes';
import { Genres } from '../../../db/models/GenresModel';
import { MovieDto } from '../dto/movie.dto';

const UpdateMovieScheme = Joi.object<MovieDto>({
  title: Joi.string().optional().max(255),
  year: Joi.number().min(0).strict().optional(),
  runtime: Joi.number().min(0).strict().optional(),
  director: Joi.string().optional().max(255),
  genres: Joi.array()
    .items(Joi.string().valid(...Object.values(Genres)))
    .min(1)
    .optional(),
  actors: Joi.string().optional(),
  plot: Joi.string().optional(),
  posterUrl: Joi.string().optional(),
});

export const UpdateMovieMiddlewareValidator = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    await UpdateMovieScheme.validateAsync(req.body);
    next(false);
  } catch (error) {
    next({ name: ErrorCodes.Validation, message: error.message, error: error });
  }
};
