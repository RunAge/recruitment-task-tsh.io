import Joi from 'joi';
import { NextFunction, Request, Response } from 'express';
import { ErrorCodes } from '../../../ErrorCodes';
import { MovieModel } from '../../../db/models/MovieModel';
import { Genres } from '../../../db/models/GenresModel';

const RecommendMovieScheme = Joi.object<MovieModel>({
  runtime: Joi.number().strict().min(0).optional(),
  genres: Joi.array()
    .items(Joi.string().valid(...Object.values(Genres)))
    .min(1)
    .optional(),
});

export const RecommendMovieMiddlewareValidator = async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    await RecommendMovieScheme.validateAsync(req.body);
    next(false);
  } catch (error) {
    next({ name: ErrorCodes.Validation, message: error.message, error: error });
  }
};
