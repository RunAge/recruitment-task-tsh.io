import { Request, Response } from 'express';
import { CreateMovieMiddlewareValidator } from './validators/CreateMovieValidator';
import { GetMovieParameterMiddlewareValidator } from './validators/GetMovieParameterValidator';
import { route, GET, POST, before, PATCH } from 'awilix-express';
import { MoviesService } from '../services/MoviesServices';
import { RecommendMovieMiddlewareValidator } from './validators/RecommendMovieValidator';
import { UpdateMovieMiddlewareValidator } from './validators/UpdateMovieValidator';

@route('/movies')
export default class MoviesApi {
  constructor(private readonly moviesService: MoviesService) {}

  @route('/')
  @GET()
  public async getAllMovies(_req: Request, res: Response): Promise<void> {
    const movies = await this.moviesService.getAllMovies();
    res.json(movies);
  }

  @route('/')
  @POST()
  @before(CreateMovieMiddlewareValidator)
  public async createMovie(req: Request, res: Response): Promise<void> {
    const movie = await this.moviesService.createMovie(req.body);
    res.status(201).json({
      code: 201,
      message: `Movie added to database with id: ${movie.id}`,
    });
  }

  @route('/recommend')
  @GET()
  @before(RecommendMovieMiddlewareValidator)
  public async getRecommendMovie(req: Request, res: Response): Promise<void> {
    const movies = await this.moviesService.getRecommendedMovies(req.body);
    res.json(movies);
  }

  @route('/:id')
  @GET()
  @before(GetMovieParameterMiddlewareValidator)
  public async getMovie(req: Request, res: Response): Promise<void> {
    const id = Number(req.params.id);
    const movie = await this.moviesService.getMovieById(id);
    res.json(movie);
  }

  @route('/:id')
  @PATCH()
  @before([GetMovieParameterMiddlewareValidator, UpdateMovieMiddlewareValidator])
  public async updateMovie(req: Request, res: Response): Promise<void> {
    const id = Number(req.params.id);
    const movie = await this.moviesService.updateMovie(id, req.body);
    res.json(movie);
  }
}
