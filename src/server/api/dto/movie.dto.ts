import { Genres } from '../../../db/models/GenresModel';

export interface MovieDto {
  title: string;
  year: number;
  runtime: number;
  genres: Genres[];
  director: string;
  actors?: string;
  plot?: string;
  posterUrl?: string;
}
