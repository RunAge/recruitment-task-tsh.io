import { Genres } from '../../../db/models/GenresModel';

export interface RecommendMovieDto {
  runtime?: number;
  genres?: Genres[];
}
