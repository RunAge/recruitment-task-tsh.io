import { Request, Response, NextFunction } from 'express';
import { ErrorCodes } from '../../ErrorCodes';

export class ErrorsHandlers {
  static async log(err: Error, _req: Request, _res: Response, next: NextFunction): Promise<void> {
    if (process.env.LOG) {
      console.error(err);
    }
    next(err);
  }

  static async notExistsError(
    err: Error,
    _req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    if (res.headersSent || err.name !== ErrorCodes.NotExists) {
      return next(err);
    }
    res.status(404).json({
      code: 404,
      message: err.message,
      error: err,
    });
  }

  static async validationError(
    err: Error,
    _req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    if (res.headersSent || err.name !== ErrorCodes.Validation) {
      return next(err);
    }
    res.status(400).json({
      code: 400,
      message: err.message,
      error: err,
    });
  }

  static async invalidJSONError(
    err: Error,
    _req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    if (res.headersSent || !/JSON/g.test(err.message)) {
      return next(err);
    }
    res.status(400).json({
      code: 400,
      message: err.message,
      error: err,
    });
  }

  static async internalError(
    err: Error,
    _req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _next: NextFunction // required to get error :(
  ): Promise<void> {
    if (res.headersSent) {
      return;
    }
    console.error(err);
    res.status(500).json({
      code: 500,
      message: 'Internal Server Error',
      error: 'Internal Server Error',
    });
  }
}
