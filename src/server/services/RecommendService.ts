import { isEmpty, intersection, omit } from 'ramda';
import { MovieModel } from '../../db/models/MovieModel';
import { Genres } from '../../db/models/GenresModel';
import { sample } from '../../utils/sample';

type MovieModelWithScore = MovieModel & { score: number };

export class RecommendService {
  private async filterByRuntime(movies: MovieModel[], runtime?: number): Promise<MovieModel[]> {
    if (!runtime) return movies;
    return movies.filter((movie: MovieModel) => {
      return runtime - 10 < movie.runtime && runtime + 10 > movie.runtime;
    });
  }

  private async scoreByGenresIntersections(
    movies: MovieModel[],
    genres?: Genres[]
  ): Promise<MovieModelWithScore[]> {
    return movies.map((movie) => {
      if (!genres || isEmpty(genres)) {
        return {
          ...movie,
          score: 1,
        };
      }
      return {
        ...movie,
        score: intersection(genres, movie.genres).length ?? 0,
      };
    });
  }

  private async filterAndSortByScore(movies: MovieModelWithScore[]): Promise<MovieModel[]> {
    return movies
      .filter((movie) => movie.score >= 1)
      .sort((movieA, movieB) => movieB.score - movieA.score)
      .map((movie) => omit(['score'], movie));
  }

  public async getRecommendedMovies(
    movies: MovieModel[],
    genres?: Genres[],
    runtime?: number
  ): Promise<MovieModel[]> {
    if (!genres && !runtime) {
      return [sample(movies)];
    }

    const filterdByRuntime = await this.filterByRuntime(movies, runtime);

    if (!genres) {
      return [sample(filterdByRuntime)];
    }

    const scoreByGenresIntersections = await this.scoreByGenresIntersections(
      filterdByRuntime,
      genres
    );

    return await this.filterAndSortByScore(scoreByGenresIntersections);
  }
}
