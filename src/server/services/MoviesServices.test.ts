import { getTestContainer } from '../../container';
import { MoviesService } from './MoviesServices';
import { testMovies } from '../../db/Test/testData';
import { MovieDto } from '../api/dto/movie.dto';
import { Genres } from '../../db/models/GenresModel';

describe('MoviesService', () => {
  let moviesService: MoviesService;
  beforeEach(() => {
    jest.resetModules();
    const container = getTestContainer();
    moviesService = container.resolve('moviesService');
  });

  it('getAllMovies', async () => {
    expect((await moviesService.getAllMovies()).length).toBe(5);
  });

  it('getMoveById', async () => {
    expect(await moviesService.getMovieById(1)).toStrictEqual(testMovies[0]);
  });

  it('createMovie', async () => {
    const movie: MovieDto = {
      title: 'test1',
      year: 1111,
      director: 'test',
      genres: [Genres.Action, Genres.Adventure],
      runtime: 100,
      actors: 'test1, test2, test3',
    };
    expect((await moviesService.createMovie(movie)).id).toBe(6);
  });

  it('updateMovie', async () => {
    const movie: Partial<MovieDto> = {
      year: 1111,
    };
    await moviesService.updateMovie(1, movie);
    expect((await moviesService.getMovieById(1)).year).toBe(1111);
  });

  it('getRecommendedMovies', async () => {
    const recommendedMovies = await moviesService.getRecommendedMovies({
      genres: [Genres.Comedy],
      runtime: 100,
    });
    expect(recommendedMovies.length).toBe(2);
    expect(recommendedMovies[0]).toStrictEqual(testMovies[0]);
  });
});
