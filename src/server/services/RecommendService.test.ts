/* eslint-disable @typescript-eslint/ban-ts-comment */
import { RecommendService } from './RecommendService';
import { testMovies } from '../../db/Test/testData';
import { Genres } from '../../db/models/GenresModel';
import { intersection } from 'ramda';

/**
 * "@ts-ignore" used to test private methods
 */

describe('RecommendService', () => {
  const recommendService: RecommendService = new RecommendService();

  it('filterByRuntime', async () => {
    // @ts-ignore
    expect((await recommendService.filterByRuntime(testMovies, 100)).length).toBe(2);
  });

  it('scoreByGenresIntersections', async () => {
    // @ts-ignore
    const scored = await recommendService.scoreByGenresIntersections(testMovies, [
      Genres.Crime,
      Genres.Drama,
    ]);
    expect(scored.some((movie) => Number(movie.score))).toBeTruthy();
  });

  it('filterAndSortByScore', async () => {
    // @ts-ignore
    const scored = await recommendService.scoreByGenresIntersections(testMovies, [
      Genres.Crime,
      Genres.Drama,
    ]);
    const highestScored = scored.sort((movieA, movieB) => movieB.score - movieA.score)[0];
    // @ts-ignore
    const filterd = await recommendService.filterAndSortByScore(scored);
    expect(filterd.length).toBe(3);
    expect(filterd[0].id).toBe(highestScored.id);
  });

  it('getRecommendedMovies should return 1 movie', async () => {
    const recommended = await recommendService.getRecommendedMovies(testMovies);
    expect(recommended.length).toBe(1);
  });

  it('getRecommendedMovies [gernes]', async () => {
    const genres = [Genres.Crime, Genres.Drama];
    const recommended = await recommendService.getRecommendedMovies(testMovies, genres);
    const hasAnyGenre = recommended.some((movie) => intersection(genres, movie.genres).length > 0);
    expect(hasAnyGenre).toBeTruthy();
  });

  it('getRecommendedMovies [runtime]', async () => {
    const runtime = 120;
    const recommended = await recommendService.getRecommendedMovies(testMovies, undefined, runtime);
    const hasCorrectRuntime = recommended.some(
      (movie) => runtime - 10 < movie.runtime && runtime + 10 > movie.runtime
    );
    expect(hasCorrectRuntime).toBeTruthy();
    expect(recommended.length).toBe(1);
  });

  it('getRecommendedMovies [gernes, runtime]', async () => {
    const genres = [Genres.Crime, Genres.Drama];
    const runtime = 120;
    const recommended = await recommendService.getRecommendedMovies(testMovies, genres, runtime);
    const hasAnyGenre = recommended.some((movie) => intersection(genres, movie.genres).length > 0);
    const hasCorrectRuntime = recommended.some(
      (movie) => runtime - 10 < movie.runtime && runtime + 10 > movie.runtime
    );
    expect(hasAnyGenre).toBeTruthy();
    expect(hasCorrectRuntime).toBeTruthy();
  });
});
