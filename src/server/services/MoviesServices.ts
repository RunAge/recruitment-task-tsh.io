import { MovieDto } from '../api/dto/movie.dto';
import { MovieModel } from '../../db/models/MovieModel';
import { MoviesRepository } from '../../db/MoviesRepository';
import { RecommendMovieDto } from '../api/dto/recommendMovie.dto';
import { RecommendService } from './RecommendService';

export class MoviesService {
  constructor(
    private readonly moviesRepository: MoviesRepository,
    private readonly recommendService: RecommendService
  ) {}
  public async getAllMovies(): Promise<MovieModel[]> {
    return await this.moviesRepository.getAll();
  }

  public async getMovieById(id: number): Promise<MovieModel> {
    return await this.moviesRepository.findById(id);
  }

  public async createMovie(movie: MovieDto): Promise<MovieModel> {
    return await this.moviesRepository.create(movie);
  }

  public async updateMovie(movieId: number, movieDto: Partial<MovieDto>): Promise<MovieModel> {
    const movieToUpdate = {
      id: movieId,
      ...movieDto,
    };
    return await this.moviesRepository.update(movieToUpdate);
  }

  public async getRecommendedMovies(options: RecommendMovieDto): Promise<MovieModel[]> {
    const movies = await this.moviesRepository.getAll();
    const recommended = await this.recommendService.getRecommendedMovies(
      movies,
      options.genres,
      options.runtime
    );
    return recommended;
  }
}
