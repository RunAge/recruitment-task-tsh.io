import express, { Request, Response } from 'express';
import http from 'http';
import { api } from './api';
import { ErrorsHandlers } from './handlers/ErrorsHandlers';

export const startServer = async (): Promise<void> => {
  const app = express();

  app.use('/api', api);
  app.get('/', async (_req: Request, res: Response) => res.send('Hi!'));

  app.use([
    ErrorsHandlers.log,
    ErrorsHandlers.invalidJSONError,
    ErrorsHandlers.notExistsError,
    ErrorsHandlers.validationError,
    ErrorsHandlers.internalError,
  ]);

  const port = Number(process.env.WEB_PORT) || 8080;
  const hostname = process.env.WEB_HOST || 'localhost';
  http.createServer(app).listen(port, hostname, () => {
    console.log(`Web server started on ${hostname}:${port}`);
  });
};
