import { PathLike, promises as fs } from 'fs';

/**
 * I didn't create abstract class for engine because most database drivers has its own engine build in.
 */

class InMemoryDatabaseEngine {
  static instance: InMemoryDatabaseEngine;
  static databasePath: PathLike;
  private internalDb: any; // engine can read any json file thats why I use any in this place.

  public static get getInstance(): InMemoryDatabaseEngine {
    if (!InMemoryDatabaseEngine.instance) {
      InMemoryDatabaseEngine.instance = new InMemoryDatabaseEngine();
    }
    return InMemoryDatabaseEngine.instance;
  }

  public collection<T>(collection: string): T[] {
    return this.internalDb[collection];
  }

  public async connect(path: PathLike): Promise<void> {
    InMemoryDatabaseEngine.databasePath = path;
    const data = (await fs.readFile(path)).toString();
    this.internalDb = JSON.parse(data);
  }

  public async save(): Promise<void> {
    const dataToWrite = JSON.stringify(this.internalDb, null, 2);
    await fs.writeFile(InMemoryDatabaseEngine.databasePath, dataToWrite);
  }
}

export const database = InMemoryDatabaseEngine.getInstance;
