import { MoviesRepository, MovieCreateData } from '../MoviesRepository';
import { MovieModel } from '../models/MovieModel';
import { database } from './InMemoryDatabaseEngine';
import { ErrorCodes } from '../../ErrorCodes';

export class InMemoryMovieRepository implements MoviesRepository {
  private movies: MovieModel[];
  constructor() {
    this.movies = database.collection<MovieModel>('movies');
  }

  public async getAll(): Promise<MovieModel[]> {
    return this.movies;
  }

  public async findById(id: number): Promise<MovieModel> {
    const result = this.movies.find((movie) => movie.id === id);
    if (!result) {
      throw { name: ErrorCodes.NotExists, message: `Movie with "${id}" not exists!` };
    }
    return result;
  }

  public async create(movie: MovieCreateData): Promise<MovieModel> {
    const toCreate = {
      ...movie,
      id: this.lastId + 1,
    };
    this.movies.push(toCreate);
    await database.save();
    return toCreate;
  }

  public async update(movie: Partial<MovieModel> & { id: number }): Promise<MovieModel> {
    const movieToUpdate = await this.findById(movie.id);
    const toUpdate = {
      ...movieToUpdate,
      ...movie,
    };
    const index = this.movies.findIndex((_movie) => _movie.id === movie.id);
    this.movies[index] = toUpdate;
    await database.save();
    return toUpdate;
  }

  private get lastId() {
    return this.movies.length;
  }
}
