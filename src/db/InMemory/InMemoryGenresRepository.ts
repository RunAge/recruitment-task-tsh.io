import { GenresRepository } from '../GenresRepository';
import { database } from './InMemoryDatabaseEngine';
import { Genres } from '../models/GenresModel';

export class InMemoryGenresRepository implements GenresRepository {
  private genres: Genres[];
  constructor() {
    this.genres = database.collection('genres');
  }

  public async getAll(): Promise<Genres[]> {
    return this.genres;
  }
}
