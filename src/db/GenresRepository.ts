import { Genres } from './models/GenresModel';

export abstract class GenresRepository {
  abstract getAll(): Promise<Genres[]>;
}
