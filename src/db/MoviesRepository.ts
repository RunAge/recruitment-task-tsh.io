import { MovieModel } from './models/MovieModel';
import { Genres } from './models/GenresModel';

export interface MovieCreateData {
  title: string;
  year: number;
  runtime: number;
  genres: Genres[];
  director: string;
  actors?: string;
  plot?: string;
  posterUrl?: string;
}

export abstract class MoviesRepository {
  abstract getAll(): Promise<MovieModel[]>;
  abstract findById(id: number): Promise<MovieModel>;
  abstract create(movie: MovieCreateData): Promise<MovieModel>;
  abstract update(movie: Partial<MovieModel> & { id: number }): Promise<MovieModel>;
}
