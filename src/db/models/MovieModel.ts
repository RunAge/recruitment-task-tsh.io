import { Genres } from './GenresModel';

export interface MovieModel {
  id: number;
  title: string;
  year: number;
  runtime: number;
  genres: Genres[];
  director: string;
  actors?: string;
  plot?: string;
  posterUrl?: string;
}
