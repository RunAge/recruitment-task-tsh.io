import { MoviesRepository, MovieCreateData } from '../MoviesRepository';
import { MovieModel } from '../models/MovieModel';
import { testMovies } from './testData';
export class TestMovieRepository implements MoviesRepository {
  private movies: MovieModel[] = testMovies;

  public async getAll(): Promise<MovieModel[]> {
    return this.movies;
  }

  public async findById(id: number): Promise<MovieModel> {
    const result = this.movies.find((movie) => movie.id === id);
    if (!result) {
      throw 'Movie not found!';
    }
    return result;
  }

  public async create(movie: MovieCreateData): Promise<MovieModel> {
    const toCreate = {
      ...movie,
      id: this.lastId + 1,
    };
    this.movies.push(toCreate);
    return toCreate;
  }

  public async update(movie: Partial<MovieModel> & { id: number }): Promise<MovieModel> {
    try {
      const movieToUpdate = await this.findById(movie.id);
      const toUpdate = {
        ...movieToUpdate,
        ...movie,
      };
      const index = this.movies.findIndex((_movie) => _movie.id === movie.id);

      this.movies[index] = toUpdate;
      return toUpdate;
    } catch (error) {
      throw error;
    }
  }

  private get lastId() {
    return this.movies.length;
  }
}
