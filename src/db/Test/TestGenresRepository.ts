import { GenresRepository } from '../GenresRepository';
import { Genres } from '../models/GenresModel';

export class TestGenresRepository implements GenresRepository {
  private genres: Genres[] = Object.values(Genres);

  public async getAll(): Promise<Genres[]> {
    return this.genres;
  }
}
