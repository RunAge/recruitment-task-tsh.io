import { database } from './db/InMemory/InMemoryDatabaseEngine';
import { join } from 'path';
import { startServer } from './server';

database
  .connect(join(process.cwd(), 'data', 'db.json'))
  .then(startServer)
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
