export enum ErrorCodes {
  Validation = 'VALIDATION',
  NotExists = 'NOTEXISTS',
}
