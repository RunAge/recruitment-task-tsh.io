/**
 * Fix movies in db.json according to task.md
 *
 * - a list of genres (only predefined ones from db file) (required, array of predefined strings)
 * - title (required, string, max 255 characters)
 * - year (required, number)
 * - runtime (required, number)
 * - director (required, string, max 255 characters)
 * - actors (optional, string)
 * - plot (optional, string)
 * - posterUrl (optional, string)
 */

import { readFileSync, writeFileSync } from 'fs';

const jsonDb = readFileSync('./data/db.orginal.json').toString();
const parsedDb = JSON.parse(jsonDb);

const fixedMovies = parsedDb.movies.map((movie: any) => ({
  ...movie,
  year: Number(movie.year),
  runtime: Number(movie.runtime),
}));

const fixedDb = JSON.stringify({ ...parsedDb, ...{ movies: fixedMovies } }, null, 2);
writeFileSync('./data/db.json', fixedDb);
